// Simple Particle System
// Daniel Shiffman <http://www.shiffman.net>
// Modified by Shawn Nikkila

// A simple Particle class

class Particle {
  PVector loc;
  PVector vel;
  PVector acc;
  PVector dest;
  
  Star parent;
  
  float lifespan;
  float max_speed = 7;
  float p_tint = random(255);
  
  boolean immortal = true;
  boolean visible  = false;
  boolean stopped  = true;
  
  boolean circular_explosion = false;

  // Another constructor (the one we are using here)
  Particle(PVector l) {
    loc = l.get();
    reset();
    
    circular_explosion = true;
  }
  
  // Resets the particle at the current position so it "explodes" outwards.
  void reset() {
    visible = true;
    parent = null;
      
    float x_v;
    float y_v;
    
    if (circular_explosion)
    {
      float x_vec = random(-1, 1);
      float y_vec = random(-1, 1);
      
      if (x_vec + y_vec > 1)
        y_vec = 1 - x_vec;
        
      float x_vec_2 = x_vec * x_vec;
      float y_vec_2 = y_vec * y_vec;
      
      x_v = (x_vec_2 - y_vec_2) / (x_vec_2 + y_vec_2);
      y_v = (2 * x_vec * y_vec) / (x_vec_2 + y_vec_2);
    }
    else
    {
      x_v = random(-1.78, 1.78);
      y_v = random(-1, 1);
    }
    
    // Boring example with constant acceleration
    acc = new PVector(0, 0, 0);
    vel = new PVector(x_v * random(max_speed - 1, max_speed), y_v * random(max_speed - 1, max_speed), 0);

    lifespan = 100000;//255;
    
    acc.sub(vel);
    acc.mult(random(0.001, 0.005));
  }
  
  // Added by Shawn Nikkila
  void setStarDestination(Star s)
  {
    if (dest == null)
      dest = new PVector();
      
    dest.x = s.loc.x;
    dest.y = s.loc.y;
    
    vel.x = (dest.x - loc.x) / 50; //dest.x / mag(dest.x, dest.y);
    vel.y = (dest.y - loc.y) / 50;
    
    acc.set(0, 0, 0);
    acc.sub(vel);
    acc.mult(random(0.001, 0.005));
    
    parent = s;
  }
  
  // Added by Shawn Nikkila.
  void setDestination(PVector p)
  {
    if (dest == null)
      dest = new PVector(p.x, p.y, 0);
    
    float d = dist(p.x, p.y, loc.x, loc.y);
    
    float r_x = (p.x - loc.x) / d;
    float r_y = (p.y - loc.y) / d;
    float p_acc = 100000 * 0.6674 / (pow(d, 2));
    
    vel.set(r_x, r_y, 0);
    vel.mult(p_acc);
    
    acc.set(0, 0, 0);
    acc.sub(vel);
    acc.mult(0.005);
  }

  void run() {
    update();
    render();
  }

  // Method to update location
  void update() {
    if (parent != null)//(dest != null && dist(dest.x, dest.y, loc.x, loc.y) <= 10)
    {
      // Stars can drift so reset the destination.
      setStarDestination(parent);
      
      if (dist(dest.x, dest.y, loc.x, loc.y) <= 10)
      {
        vel.set(0, 0, 0);
        
        parent.particles.add(this);
        
        dest    = null;
        parent  = null;
        visible = false;
      }
    }
      
    vel.add(acc);
    loc.add(vel);
      
    if (mag(vel.x, vel.y) < 0.1 && mag(vel.x, vel.y) > -0.1)
    {
      acc.set(0, 0, 0);
      vel.set(0, 0, 0);
      
      if (!stopped)
        p_tint = random(50, 175);
        
      stopped = true;
    }
    else
    {
      stopped = false;
    }
    
    lifespan -= 2.0;
  }

  // Method to display
  void render() {
    if (visible)
    {
      imageMode(CENTER);
      
      tint(p_tint);//lifespan);
        
      image(basic_particle,loc.x,loc.y, 32, 32);
    }
  }
  
  // Is the particle still useful?
  boolean dead() {
    if (lifespan <= 0.0 && !immortal) {
      return true;
    } else {
      return false;
    }
  }
}


