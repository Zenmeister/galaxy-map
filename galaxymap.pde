// Galaxy Map
// File:        galaxymap.pde
// Author:      Shawn Nikkila
// Description: This sketch represents mouse clicks as stars forming. Mouse
// clicks that are close to each other merge together to form larger stars.
// Once stars become too large or enough time elapses, the stars go
// supernova.

import fullscreen.*;
import japplemenubar.*;

import processing.opengl.*;
import javax.media.opengl.*;
import java.util.Timer;


// --------------------------------------------------------------
// CONSTANTS
// --------------------------------------------------------------
final int READ_DELAY = 100;
final int STAR_TTL   = 1000;

// Data file to read which contains the times and locations of the mouse clicks.
final String DATA = "dow\\022712181649_mouse.txt";
//final String DATA = "working_irb\\022712133915_mouse.txt";
//final String DATA = "minesweeper\\022512135506_mouse.txt";


// --------------------------------------------------------------
// VARIABLES
// --------------------------------------------------------------
// OpenGL stuff
PGraphicsOpenGL pgl;
GL gl;

// Modified particle system class from Daniel Shiffman.
ParticleSystem ps;

// Messages
AnimatedMessage event_msg;
Popup popup;

// Regulates how often mouse clicks are read from the data file.
Timer read_timer;

// From the full screen library.
SoftFullScreen fs;

PFont font;

PImage basic_particle;
PImage cool_particle_1;
PImage cool_particle_2;
PImage warm_particle_1;
PImage warm_particle_2;

volatile ArrayList<Star> stars;
volatile String vis_time = "";

BufferedReader reader;
String data;

int outlier_particles;


// Responsible for reading the data file and creating stars for each
// mouse click recorded in the file.
class ReadTask extends TimerTask 
{
  public void run() 
  {
    if (reader == null)
      reader = createReader(DATA);
    else 
    {
      try 
      {
        // Read the next line in the file.
        data = reader.readLine();
        
        if (data != null)
        {
          // Data file is comma separated.
          String [] columns = data.split(", ");
          
          // Get the type of mouse click (left, right, or no mouse click)
          String click = columns[3];
          
          // X and Y coordinates of the mouse click normalized to fit the 
          // sketch window.
          int x = Integer.parseInt(columns[1]) * width / 1980;
          int y = Integer.parseInt(columns[2]) * height / 1080;
          
          for (int i = 0; i < stars.size(); i++)
            stars.get(i).ttl--;
          
          // Color of the star depends on if it was a left or right mouse
          // click. The left mouse click will create a star that is of a 
          // warm color whereas a right mouse click will create a star that
          // is of a cool color.
          if (click.equals("Left"))
            createStar(x, y, "warm");
          else if (click.equals("Right"))
            createStar(x, y, "cool");
          
          vis_time = columns[0];
        }
      }
      catch (IOException ex) 
      {
        ex.printStackTrace();
      }
    }
  }
}


// Code for initializing the sketch.
void setup() 
{
  size(1920, 1080, OPENGL);
  frameRate(30);
  
  // Set the processing sketch to be full screen.
  fs = new SoftFullScreen(this);
  fs.enter();

  font = loadFont("neuropol-48.vlw");
  
  // Create an alpha masked image to be applied as the particle's texture
  cool_particle_1 = loadImage("particle4.png");
  cool_particle_2 = loadImage("particle3.png");
  warm_particle_1 = loadImage("particle1.png");
  warm_particle_2 = loadImage("particle2.png");
  basic_particle  = loadImage("particle1.png");

  // Instantiate the particle system.
  ps = new ParticleSystem(10000, new PVector(width, height));
  
  smooth();

  // Set up some OpenGL stuff.
  pgl = (PGraphicsOpenGL) g;
  gl = pgl.gl;
  
  // Set up the read task which will read from the data file at a set
  // interval.
  read_timer = new Timer();
  read_timer.scheduleAtFixedRate(new ReadTask(), 10000, READ_DELAY);

  // Data structure to store the star objects.
  stars = new ArrayList<Star>();
  
  event_msg = new AnimatedMessage(width * 0.35, 48);
  event_msg.font_size = 24;
  event_msg.setText("in the beginning");
  
  // Reusable popup message for when stars are created.
  popup = new Popup();
}

void draw() 
{
  pgl.beginGL();
  gl.glEnable(GL.GL_BLEND);
  gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE);

  // Set the background to black.
  background(0);

  ps.run();
  
  PVector grav_force = new PVector();
  
  synchronized(stars)
  {
    Iterator<Star> i_stars = stars.iterator();
    
    // Iterate through every star to update it.
    while (i_stars.hasNext())
    {
      Star s = i_stars.next();
      
      // If a star's time to live has expired or it has been destroyed, then 
      // make it go supernova.
      if (s.ttl <= 0 || s.destroyed)
      {
        if (!s.destroyed)
          s.superNova(null);
          
        i_stars.remove();
      }
      else
      {
        Iterator<Star> j_stars = stars.iterator();
        
        // Every star is effected by every other star's gravitational pull.
        // Therefore, iterate through every other star to calculate how
        // much pull is occurring in which direction.
        while(j_stars.hasNext())
        {
          Star s2 = j_stars.next();
  
          // Ignore stars that are in the process of being destroyed.
          if (s2 == s || s2.destroyed)
            continue;
            
          // Calculate the gravitational pull using Newton's Universal Law 
          of Gravity
          float d = dist(s2.loc.x, s2.loc.y, s.loc.x, s.loc.y);
          
          // If two stars are very close to each other, then they should
          // merge with each other so that the smallest star gets sucked
          // into the other one.
          if (d < 10)
          {
            s.vel.set(0, 0, 0);
            
            if (s.particles.size() > s2.particles.size())
              s2.superNova(s);
            else
              s.superNova(s2);
              
            // Popup informing that the two stars are merging.
            popup.showMessage("STARS MERGING", 
                "Scanners have detected two stars merging at sector <" + 
                round(s.loc.x / 2) + ", " + round(s.loc.y / 2) + 
                ">", s.loc.x / 2, s.loc.y / 2);
          }
          else
          {
            // Otherwise, calculate the gravitational pull between the
            // two bodies and adjust accordingly.
            float r_x = (s2.loc.x - s.loc.x) / d;
            float r_y = (s2.loc.y - s.loc.y) / d;
            float acc = s2.particles.size() * 0.06674 / (pow(d, 2));
            
            grav_force.set(r_x, r_y, 0);
            grav_force.mult(acc);
            
            if (mag(s.vel.x + grav_force.x, s.vel.y + grav_force.y) < 1)
              s.vel.add(grav_force);
          }
        }
        
        s.render();
      }
    }
  }
  
  // Info box at the top of the screen.
  gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_SRC_ALPHA);
  fill(0, 60);
  stroke(255, 0, 0);
  rect(0, 0, width * 2, height * 0.1);
  
  pgl.endGL();

  textFont(font, 32);
  fill(255);
  text("galaxy map v.011A", 10, 48);
  
  textFont(font, 24);
  fill(255);
  text(vis_time, width * 0.75, 48);
  
  event_msg.render();
  popup.render();
}


// Helper method for creating a star at a given X, Y coordinate.
void createStar(int x, int y, String type)
{
  float r = 200.0;
  
  x *= 2;
  y *= 2;
  
  Star s = new Star(x, y);
  
  int selection = round(random(1));
  
  if (type.equals("warm"))
  {
    if (selection == 0)
      s.particle_image = warm_particle_1;
    else
      s.particle_image = warm_particle_2;
  }
  else if (type.equals("cool"))
  {
    if (selection == 0)
      s.particle_image = cool_particle_1;
    else
      s.particle_image = cool_particle_2;
  }
  
  // When stars are formed, they pull in the surrounding particles that
  // are close enough to this. 
  for (int i = 0; i < ps.particles.size(); i++)
  {
    Particle p = ps.particles.get(i);
    
    if (dist(p.loc.x, p.loc.y, x, y) <= r && p.vel.mag() < 1 && p.parent == null)
    {
      p.p_tint = 255;
      p.setStarDestination(s);
    }
    else
    {
      if (p.vel.mag() == 0)
        p.setDestination(s.loc);
    }
  }
  
  // Add the new star to the list.
  synchronized(stars)
  {
    stars.add(s);
  }
  
  // Display a message and display the popup announching the creation of
  // the star.
  event_msg.setText("Star birth detected at sector <" + x / 2 + ", " + y / 2 + ">");
  popup.showMessage("STAR BIRTH DETECTED", "Event detected in sector <" + x / 2 + ", " + y / 2 + ">", x / 2, y / 2);
}


// Stars can be created by just clicking on the screen as well for
// testing purposes or for just goofing around.
void mouseClicked() 
{
  if (mouseButton == LEFT)
    createStar(mouseX, mouseY, "warm");
  else if (mouseButton == RIGHT)
    createStar(mouseX, mouseY, "cool");
}

