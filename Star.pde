// Galaxy Map
// File:        Star.pde
// Author:      Shawn Nikkila
// Description: Reach for the stars. Responsible for rendering the stars and
// updating them.
class Star
{
  // --------------------------------------------------------------
  // CONSTANTS
  // --------------------------------------------------------------
  final int DEFAULT_MASS  = 10;
  final int CRITICAL_MASS = 2000;
  
  
  // --------------------------------------------------------------
  // INSTANCE VARIABLES
  // --------------------------------------------------------------
  PImage particle_image;
  
  PVector loc;
  PVector vel;
  
  // Each star holds a certain number of particles which is proportionate
  // to the size of the star.
  ArrayList<Particle> particles;
  
  float mass;
  float star_radius;
  float time_accumulator;
  
  int ttl = STAR_TTL;

  boolean destroyed;
  
  
  // --------------------------------------------------------------
  // CONSTRUCTORS
  // --------------------------------------------------------------
  // Constructor takes in the X and Y coordinates for the star.
  Star(float x, float y)
  {
    loc = new PVector(x, y);
    particles = new ArrayList<Particle>();
    
    vel = new PVector();
  }
  
  
  // --------------------------------------------------------------
  // INSTANCE METHODS
  // --------------------------------------------------------------
  // Adds a particle to this star.
  void addParticle(Particle p)
  {
    particles.add(p);
    mass = particles.size();
    
    if (mass < DEFAULT_MASS)
      mass = DEFAULT_MASS;
  }
  
  
  // Make the star go "supernova" which actually means one of two things.
  // If there is no destination star, then the star explodes with the particles
  // shooting off in different directions. Otherwise, the particles from this star 
  // merge into the destination star.
  void superNova(Star dest)
  {
    Iterator<Particle> it = particles.iterator();
    
    // Iterate through all the particles.
    while (it.hasNext())
    {
      // Set the location, speed, and reset the particle.
      Particle p = it.next();
      p.loc.x = loc.x;
      p.loc.y = loc.y;
      p.max_speed = 5;
      p.reset();
      
      if (dest != null)
        p.setStarDestination(dest);
      
      it.remove();
    }
    
    destroyed = true;
    star_radius = 0;
  }
  
  
  // Render the star.
  void render()
  {
    imageMode(CENTER);
    tint(255);
    
    // Move the star if it has a velocity.
    loc.add(vel);
    
    // Make sure the star's radius is not below the minimum radius.
    if (star_radius < 200)
      star_radius = 200;
      
    // If the star's radius is greater than the critical mass, then 
    // set it to go supernova.
    if (star_radius > CRITICAL_MASS)
    {
      superNova(null);
      popup.showMessage("SUPERNOVA DETECTED", 
          "A supernova has been detected at sector <" + 
          round(loc.x / 2) + ", " + round(loc.y / 2) + 
          ">", loc.x / 2, loc.y / 2);
    }

    // Grow the star if the number of particles contained is greater
    // than the radius of the star.
    if (star_radius < particles.size())
      star_radius+=10;
      
    image(particle_image, loc.x, loc.y, star_radius, star_radius);
  }
}
