Galaxy Map
Author: Shawn Nikkila

A Processing sketch which represents the desktop as a galaxy with
mouse activity data representing star formation. Data was collected
using a tool that I implemented that intercepts mouse events using the
Windows API. This was implemented for fun and for a small class project.

The Particle.pde and ParticleSystem.pde files are (C) Daniel Shiffman with
slight modifications made by myself. Updated versions of the original files
can be found at:

http://www.learningprocessing.com/examples/chapter-23/example-23-2/.

The base particle texture was taken from http://www.flight404.com/.



