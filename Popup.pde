// Galaxy Map
// File:        Popup.pde
// Author:      Shawn Nikkila
// Description: Displays a message at the top of the screen which is rendered
// one character at a time to emulate a science fiction typing effect.
class Popup
{
  // --------------------------------------------------------------
  // CONSTANTS
  // --------------------------------------------------------------
  final float TTL = 6.0;
  
  
  // --------------------------------------------------------------
  // INSTANCE VARIABLES
  // --------------------------------------------------------------
  AnimatedMessage header;
  AnimatedMessage message;
  
  PVector loc;
  PShape message_box;
  
  float accumulator;
  boolean visible;
  
  
  // --------------------------------------------------------------
  // CONSTRUCTORS
  // --------------------------------------------------------------
  // Default constructor.
  Popup() 
  {
    // Header and message body are both AnimatedMessage instances.
    header = new AnimatedMessage(); 
    header.font_size = 18;
    
    message = new AnimatedMessage();
    message.font_size = 12;
    message.dim.set(255, 70, 0);
    message.show_cursor = false;
    
    loc = new PVector();
    
    message_box = loadShape("popup.svg");
  }
  
  
  // --------------------------------------------------------------
  // INSTANCE METHODS
  // --------------------------------------------------------------
  // Display the given message.
  void showMessage(String h_content, String m_content, float x, float y)
  {
    header.setText(h_content);
    message.setText(m_content);
    
    loc.set(x, y, 0);
    
    header.loc.set(x + 20, y + 30, 0);
    message.loc.set(x + 65, y + 52, 0);
    
    visible = true;
    
    accumulator = 0;
  }
  
  
  // Render the popup box.
  void render()
  {
    // Update the accumulator.
    accumulator += 1 / frameRate;
    
    // If the accumulated time is greater than the time to live for
    // the popup message, then make it disappear by setting the visible
    // flag to false.
    if (accumulator > TTL)
    {
      accumulator = 0;
      visible = false;
    }
    
    // Show the popup box if it is supposed to be visible.
    if (visible)
    {
      smooth();
      shape(message_box, loc.x, loc.y, 320, 128);
      header.render();
      message.render();
    }
  }
}
