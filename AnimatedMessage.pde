// Galaxy Map
// File:        AnimatedMessage.pde
// Author:      Shawn Nikkila
// Description: Displays a message at some given coordinates which is rendered
// one character at a time to emulate a science fiction typing effect.

class AnimatedMessage
{
  // --------------------------------------------------------------
  // CONSTANTS
  // --------------------------------------------------------------
  final float CHAR_DELAY = 0.05;
  final float CURS_DELAY = 0.4;
  
  
  // --------------------------------------------------------------
  // INSTANCE VARIABLES
  // --------------------------------------------------------------
  PVector loc;
  PVector dim;
  String content = "";
  
  float t_accum = 0;
  
  int char_index;
  int font_size = 32;
  
  boolean show_cursor = true;
  
  
  // --------------------------------------------------------------
  // CONSTRUCTORS
  // --------------------------------------------------------------
  AnimatedMessage(float x, float y)
  {
    loc = new PVector(x, y, 0);
    dim = new PVector();
  }
  
  AnimatedMessage() 
  {
    loc = new PVector(); 
    dim = new PVector();
  }
  
  
  // --------------------------------------------------------------
  // INSTANCE METHODS
  // --------------------------------------------------------------
  // Sets the text for the message.
  public void setText(String value)
  {
    content = value;
    char_index = 0;
    t_accum = 0;
  }
  
  
  // Display the text.
  public void render()
  {
    // Update the accumulator.
    t_accum += 1 / frameRate;
    
    // If the accumulated time is greater than the interval time
    // in which to display the next character, then display it.
    if (t_accum > CHAR_DELAY)
    {
      if (char_index < content.length())
        char_index++;
      
      t_accum = 0;
    }
    
    // Get the string to be displayed.
    String disp_string = content.substring(0, char_index);
    
    textFont(font, font_size);
    fill(255);
    
    if (dim.mag() == 0)
      text(disp_string, loc.x, loc.y);
    else
      text(disp_string, loc.x, loc.y, dim.x, dim.y);
    
    if (char_index < content.length())
    {
      stroke(255);
      fill(255);
      
      if (show_cursor)
        rect(loc.x + textWidth(disp_string), loc.y - font_size, 2, font_size);
    }
  }
}
